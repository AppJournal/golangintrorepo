//Project : A simple calculator in Go Language which supports Addition, multiplication,
//			subtraction, and division.
package main

import "fmt"

func add(x, y int) int {
	out := x + y
	return out
}

func subtract(x int, y int) int {
	return x - y
}

func main() {
	num1 := 100
	num2 := 299

    //test
	fmt.Println(add(num1, num2))
	
	fmt.Println(subtract(10, 20))
}
